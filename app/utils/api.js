import request from './request'
let baseUrl = 'http://localhost:4000/api/v1';

export default  {
  httpGet: (url, data) => {
    if (typeof data === 'object') {
      data = JSON.stringify(data)
    }
      return request(baseUrl+url,data)
  },
  httpPost: (url, data) => {
    if (typeof data === 'object') {
      data = JSON.stringify(data);
    }
    return request(
      baseUrl+url,
      {
        method: "POST",
        body: data
      })
  },
  httpPut: (url, data) => {
    if (typeof data === 'object') {
      data = JSON.stringify(data);
    }
    return request(
      baseUrl+url,
      {
        method: "PUT",
        body: data
      })
  },
  httpDelete: (url) => {
    return request(
      baseUrl+url,
      {
        method: "DELETE"
      })
  }
}