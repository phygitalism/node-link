
import _ from 'lodash';
import io from 'socket.io-client';
let connectTimeout;
let pingInterval;
let  socket;
let isConnected = false;

let baseUrl = 'http://localhost:4000';

function connect(done){
	if(typeof io == 'undefined'){
		connectTimeout = setTimeout(_.partial(connect,done),1000);
		return;
	}

	let socketOpts = {
		'forceNew': false
	};
	// if(storage.getItem(STORAGE_TOKEN_KEY)){
	// 	socketOpts.query =  {token: storage.getItem(STORAGE_TOKEN_KEY)}
	// }

	socket = io.connect(baseUrl, socketOpts);

	//encodeURIComponent(
	// socket.on('TURN_FULL', (json) => {
	// 	console.log('FULL_TURN',json);
  //
  // });




	onConnect();
	if(done){
		return  done(socket);
	}else{
		return;
	}

}

function onConnect(){
	isConnected = true;
	console.log('on connect --');

	socket.on('connect', (a) => {
    console.log('on connect ++');
		// dispatch(uiActions.hideDisconnectIcon());
	})
	socket.on('disconnect', onDisconnect);
	// if(pingInterval){
	// 	clearTimeout(pingInterval)
	// }
	// pingInterval = setInterval(function(){
	// 		socket.emit('ping',{});
	// },pingTime);
	// dispatch(uiActions.hideDisconnectIcon());
	//dispatch(uiActions.showDisconnectIcon());
}

function onDisconnect(){
	isConnected = false;
	if(connectTimeout){
		clearInterval(connectTimeout);
	}
	if(pingInterval){
		clearTimeout(pingInterval)
	}
	// dispatch(uiActions.showDisconnectIcon());
}

function disconnect(){
	onDisconnect()
}

function emit(cmd,data){
	console.log('emit ',cmd,socket);
	if(socket){
		console.log('emit +');
		socket.emit(cmd,data)
	}
}

// function auth(token){
// 	console.log('auth');
// 	if(socket) {
// 		socket.io.opts.query = 'token='+encodeURIComponent(storage.getItem(STORAGE_TOKEN_KEY));
// 	}
// 	emit('auth', token)
// }
//
// function logout(){
// 	emit('logout');
// }
connect();
export default {

	connect:connect,
	disconnect:disconnect,
	emit:emit,
	getSocket:function(done){
		console.log('gs ',isConnected)
		if(isConnected){
			done(socket)
		}else{
			io.on('connection',done)
		}

	}
}
