import React from 'react';
import { FormattedMessage } from 'react-intl';

import A from 'components/A';
import LocaleToggle from 'containers/LocaleToggle';
import Wrapper from './Wrapper';
import messages from './messages';

function Footer() {
  return (
    <Wrapper>
      <section>
        <div>Phygitalism.CPS Prototype</div>
      </section>
      <section>

      </section>
      <section>
        2017
      </section>
    </Wrapper>
  );
}

export default Footer;
