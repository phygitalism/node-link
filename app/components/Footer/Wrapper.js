import styled from 'styled-components';

const Wrapper = styled.footer`
  display: flex;
  justify-content: space-between;
  padding: 0.5em 1em;
  position: fixed;
  bottom:0;
  width: 100%;
  font-size:1em;
  background-color: rgba(0,0,0,0.2);
  color: black;
  
`;

export default Wrapper;
