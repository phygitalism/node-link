import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import A from './A';
import Img from './Img';
import NavBar from './NavBar';
import HeaderLink from './HeaderLink';
import Banner from './banner.jpg';
import messages from './messages';

const HeaderBody = styled.div`
  min-width: 100%;
  position: absolute;
  z-index:11;
  padding: 20px;
  display: flex;
  height: 200px;
  color: black;
  font-size:20px;
  pointer-events:none;
  flex-direction: column;
  background: transparent; /* For browsers that do not support gradients */
  background: -webkit-linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0,0)); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0,0)); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0,0)); /* For Firefox 3.6 to 15 */
  background: linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0,0)); /* Standard syntax */
`;

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <HeaderBody>
        CPS Admin
      </HeaderBody>
    );
  }
}

export default Header;
