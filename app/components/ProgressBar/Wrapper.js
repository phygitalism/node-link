import styled from 'styled-components';

export default styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100px;
  background-color:red;
  transition: all 500ms ease-in-out;
  z-index: 9999;
`;
//visibility: ${(props) => props.hidden ? 'hidden' : 'visible'};