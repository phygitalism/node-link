import styled from 'styled-components';

const ContentWrapper = styled.div`
  margin: 0;
  padding: 6px;
  height:350px;
  overflow-y:scroll; 
  position: relative;
`;

export default ContentWrapper;
