import React from 'react';

import Cmd from './Cmd';
import Port from './Port';
import Module from './Module';
import Wrapper from './Wrapper';
import API from '../../utils/api'
import ContentWrapper from './ContentWrapper';
import {Tabs, Tab} from 'material-ui/Tabs';
import {FlatButton} from 'material-ui';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import JSONTree from 'react-json-tree'
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import JsonDiffReact from 'jsondiffpatch-for-react';


let FABstyle = {
  position:'absolute',
  bottom:'20px',
  right:'20px',
  zIndex:'11'
};


class Console extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props){
    super(props)
    this.state = {
      tabId:'events',
      frontJson:{},
      eventsList:[],
      backJson:{}
    };


  }
// <Module>{item.msg.module.name}({item.msg.module.id.substr(0,8)})</Module> : <Port>{item.msg.port.label}({item.msg.port.id.substr(0,8)})</Port>
// <Port>{item.msg.port.label}({item.msg.port.id.substr(0,8)})</Port>
  renderItems(){
    console.log('a',this.props.eventsList);
    return this.props.eventsList.map((item, i) => {

      return (<div key={i} className="console-item" id="consolescroll">
        <span>{i}. </span>
        <Module>{item.msg.source.module.name}({item.msg.source.module.id.substr(0,8)})</Module> => <Module>{item.msg.target.module.name}({item.msg.target.module.id.substr(0,8)})</Module>
        <span>={String(item.msg.data)}=</span>
        </div>)
    });
  }

  clearEventsList(){
    this.props.onClearEventsList();
  }

  getFrontJson(){
    //SERIALIZE
      this.setState({...this.state,frontJson:this.props.model.serializeDiagram()})
  }

  getBackJson(){
    //API
    API.httpGet('/stage')
      .then((backJson) => {
        this.setState({...this.state,backJson:backJson})
      })
  }

  refreshDiff(){
    this.getFrontJson();
    this.getBackJson();
  }

  handleChange = (value) => {
    this.setState({
      tabId: value,
    });
  };

  render() {
    let button;
    switch (this.state.tabId){
      case 'events':
        button = (<FloatingActionButton
          style={FABstyle}
          mini={true}
          onClick={this.clearEventsList.bind(this)}>
          <DeleteIcon />
        </FloatingActionButton>);
        break;
      case 'front':
        button = (<FloatingActionButton
          style={FABstyle}
          mini={true}
          onClick={this.getFrontJson.bind(this)}>
          <RefreshIcon />
        </FloatingActionButton>);
        break;
      case 'back':
        button = (<FloatingActionButton
          style={FABstyle}
          mini={true}
          onClick={this.getBackJson.bind(this)}>
          <RefreshIcon />
        </FloatingActionButton>);
        break;
      case 'fvsb':
        button = (<FloatingActionButton
          style={FABstyle}
          mini={true}
          onClick={this.refreshDiff.bind(this)}>
          <RefreshIcon />
        </FloatingActionButton>);
        break;
    }
    return (
      <Wrapper className="console">
        <Tabs value={this.state.tabId} onChange={this.handleChange}>
          <Tab label="Events" value="events" >
            <ContentWrapper>
              {this.renderItems()}
            </ContentWrapper>
          </Tab>
          <Tab label="State of front" value="front">
            <ContentWrapper>
              <JSONTree data={ this.state.frontJson } />
            </ContentWrapper>
          </Tab>
          <Tab label="State of back" value="back">
            <ContentWrapper>
              <JSONTree data={ this.state.backJson } />
            </ContentWrapper>
          </Tab>
          <Tab label="Front vs Back diff" value="fvsb">
            <ContentWrapper>

              <JsonDiffReact
                right={this.state.backJson}
              left={this.state.frontJson}
              show={true}

            />
            </ContentWrapper>
          </Tab>
        </Tabs>
        {button}
      </Wrapper>
    );
  }
}

Console.propTypes = {
  items: React.PropTypes.array
};

export default Console;
