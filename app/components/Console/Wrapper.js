import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 0;
  width: calc(100% - 250px);
  height: 400px;
  overflow-y: scroll;
  overflow-x: hidden;
  color: white;
  font-size: 10px;
  bottom: 0;
  position: absolute;
  z-index:999;
  background-color: rgba(0, 0, 0, 0.7);

`;

export default Wrapper;
