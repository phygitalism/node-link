/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import "../../styles/diagram.scss"
import "../../styles/console.scss"

import React from 'react';
import ReactDOM from 'react-dom'
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Divider from 'material-ui/Divider';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Toggle from 'material-ui/Toggle';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardText} from 'material-ui/Card';


import API from '../../utils/api'
import socket from '../../utils/sockets'
import {default as zoo, PortTypedFactory, zooflat} from 'cps-zoo'
import { TYPES } from 'cps-module'
// console.log('++? ',zooFront, PortTypedFactory)
import { makeSelectRepos, makeSelectLoading, makeSelectError } from 'containers/App/selectors';
import Console from 'components/Console';
import H2 from 'components/H2';
import AtPrefix from './AtPrefix';
import CenteredSection from './CenteredSection';
import Form from './Form';
import Input from './Input';
import Section from './Section';
import Header from 'components/Header';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';


// import {DiamondNodeModel} from "../../storm-nodes/DiamondNodeModel";
// import {DiamondWidgetFactory} from "./DiamondWidgetFactory";
// import {DiamondNodeFactory, DiamondPortFactory} from "./DiamondInstanceFactories";

import * as SRD from "storm-react-diagrams";

let FABstyle = {
  position:'fixed',
  top:'20px',
  left:'20px',
  zIndex:'11'
};



function subscribeModel(model){
  model.addListener({
    actionStoppedFiring:(a,b,c)=>{
      console.log('actionStoppedFiring',a,b,c);
    },
    linksUpdated:(entity, isAdded) => {
      console.log('link updated',isAdded?'added':'removed', entity.serialize());
      if(isAdded) {
      //   API.httpPost('/links',entity.serialize())
      } else {
        API.httpDelete('/links/'+entity.id)
      }
      entity.addListener({
        targetPortChanged:(a,b) => {
          console.log('link targetPortChanged',a,b);
          API.httpPost('/links',entity.serialize())
        }
      });
    },
    nodesUpdated: (entity, isAdded) => {
      console.log('node',isAdded?'added':'removed', entity.serialize());
      if(isAdded){
        // API.httpPost('/nodes',JSON.stringify(entity.serialize()));
      } else {
        API.httpDelete('/nodes/'+entity.id);
      }
    }
  });
}


function subscribeNode(node){
  node.addListener({
    selectionChanged:(entity, isSelected) => {
      console.log('Node selectionChanged', isSelected, entity)
      if(isSelected){
        this.setState({...this.state, selectedNode:entity});
      }else{
        this.setState({...this.state, selectedNode:null});
      }
    }
  })
}

function subscribeEngine(engine){
  engine.addListener({
    offsetUpdated:(entity, x, y) => {
      console.log('Node moved engine', entity, x, y)
    }
  })
}

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */

  constructor(props) {
    super(props);

    // 1) setup the diagram engine
    let engine = new SRD.DiagramEngine();
    engine.registerNodeFactory(new SRD.DefaultNodeFactory());
    engine.registerLinkFactory(new SRD.DefaultLinkFactory());

    engine.registerInstanceFactory(new SRD.DefaultNodeInstanceFactory());
    engine.registerInstanceFactory(new SRD.DefaultPortInstanceFactory());
    engine.registerInstanceFactory(new SRD.LinkInstanceFactory());

    subscribeEngine(engine);

    socket.getSocket((socket)=>{
      this.socket = socket;

      this.socket.on('signal',(msg) => {
        console.log('signal', msg);
        let trgNode =  _.find(this.state.model.getNodes(),{id:msg.target.module.id});
        let srcNode =  _.find(this.state.model.getNodes(),{id:msg.source.module.id});
        console.log('signal!',msg, ":", trgNode,srcNode );
        if(trgNode && srcNode){
          msg.target.port = _.find(trgNode.ports, {id:msg.target.port});
          msg.source.port = _.find(srcNode.ports, {id:msg.source.port});
          this.setState({...this.state,eventList:this.state.eventList.concat([{cmd:'signal',msg}])});
          console.log('++',trgNode.setData);
          trgNode.setData(msg.data);
          trgNode.setActivity(this.redrawDiagram.bind(this));
        }

        if(this.state.consoleScroll){
          this.state.consoleScroll.scrollTop = this.state.consoleScroll.scrollHeight;
        }

      });


      this.socket.on('moduleConfigChange', (msg) => {

        let node =  _.find(this.state.model.getNodes(),{id:msg.moduleId});
        if(msg.config){
          if(typeof msg.config == 'string'){
            try{
              msg.config = JSON.parse(msg.config)
            } catch (e){
              console.log('err ',e)
            }
          }
        }
        console.log('moduleConfigChange',msg, node);
        if(node){
          //if config - add, if no - delete
          if(msg.config){
            // msg.config.color='rgb(222,0,0)'
            // console.log('moduleConfigChange:::', node);
            // console.log('msg.config',JSON.stringify(msg.config));
            node.deSerialize(msg.config);
            _.forEach(msg.config.ports,(port) => {
              let portOb = this.state.engine.getInstanceFactory(port._class).getInstance();
              portOb.deSerialize(port);
              node.addPort(portOb);
            });
            // console.log('moduleConfigChange:::+', JSON.stringify(node));
          } else {
            //TODO delete
          }

        }else{
          //add one
          if(msg.config){
            let node = zooflat[_.upperFirst(_.camelCase(msg.config.type))];
            let n =  new node.NodeModel(msg.config, msg.config.id);

            subscribeNode.bind(this)(n);
            this.state.model.addNode(n);
          }else{
            //do nothing
          }
        }
        console.log('moduleConfigChange _');
        this.redrawDiagram();

      })
    })

    // _.each(zoo,()=>{
    //
    // });

    //2) setup the diagram model
    let model = new SRD.DiagramModel();
    let nodes = [];

    engine.registerInstanceFactory(new PortTypedFactory());
    _.each(zooflat,(node)=>{
        engine.registerNodeFactory(new node.WidgetFactory());
        engine.registerInstanceFactory(new node.NodeFactory());
    });
    //
    //   let n =  new node.NodeModel();
    //   n.x = 400;
    //   n.y = 100;
    //   model.addNode(n);


    //3-A) create a default node
    // var node1 = new SRD.DefaultNodeModel("Switcher","rgb(0,192,255)");
    // var port1 = node1.addPort(new SRD.DefaultPortModel(false,"out-1","Out"));
    // node1.x = 100;
    // node1.y = 100;
    //
    // //3-B) create another default node
    // var node2 = new SRD.DefaultNodeModel("Led","rgb(192,255,0)");
    // var port2 = node2.addPort(new SRD.DefaultPortModel(true,"in-1","In"));
    // node2.x = 400;
    // node2.y = 100;
    //
    // node2.addListener({
    //   entityRemoved: (n,a,b) => {
    //     console.log('REMOCED',n,a,":",b)
    //   },
    //   selectionChanged: (n,a,b) => {
    //     console.log('SELECTED',n,a,":",b)
    //   },
    // });




    // nodes.push(node1);
    // nodes.push(node2);
    //3-C) link the 2 nodes together
    // var link1 = new SRD.LinkModel();
    // link1.setSourcePort(port1);
    // link1.setTargetPort(port2);

    //4) add the models to the root graph
    // model.addNode(node1);
    // model.addNode(node2);
    // model.addLink(link1);

    //5) load model into engine
    engine.setDiagramModel(model);
    this.state = { engine, model, nodes, addMenuOpened:false, inspectorOpened:true, eventList:[] };
  }

  nodeSelectHandle(node){
    console.log('\n\n!!! Node Selected', node)
  }

  componentDidMount() {

    API.httpGet('/stage')
      .then((stageJson) => {
        let model2 = new SRD.DiagramModel();
        this.state.engine.setDiagramModel(model2);
        model2.deSerializeDiagram(stageJson, this.state.engine);
        subscribeModel(model2);
        this.setState({model:model2});
        _.each(model2.getNodes(),subscribeNode.bind(this));
        this.forceUpdate()
      });


    // const nn = ReactDOM.findDOMNode(this.messagesEnd);
    // console.log('nnnn   ', nn, this.messagesEnd);
    // nn.scrollIntoView({behavior: "smooth"});
    // this.setState({...this.state, consoleScroll:nn});
  }


  renderD(){
    return React.createElement(SRD.DiagramWidget,{diagramEngine: this.state.engine,
      allowLooseLinks:false,
      actionStoppedFiring:(base) => {
        if(base.selectionModels && base.selectionModels[0]){
          if(base.constructor.name == 'MoveItemsAction' && base.selectionModels[0].model.constructor.name === 'TypedNodeModel'){
            API.httpPut('/nodes/'+base.selectionModels[0].model.id, _.pick(base.selectionModels[0].model,['x','y']))
              .then((resp)=>{
                console.log('res',resp)
              });
          }
        }
      }
    });
  }

  redrawDiagram(){
    this.forceUpdate();
    console.log('this.state.engine.repaintCanvas +',this.state.engine.repaintCanvas)
    this.state.engine.repaintCanvas();
  }

  addClickHandle(node){
    // let n =  new node.NodeModel(node.config);
    // n.x = 400;
    // n.y = 100;
    // subscribeNode.bind(this)(n);
    // this.state.model.addNode(n);
    API.httpPost('/nodes', {type:node.config.type})
      .then((resp)=>{
        console.log('nodes resp', resp);
        // let n =  new node.NodeModel(resp, resp.id);
        // n.x = 400;
        // n.y = 100;
        // subscribeNode.bind(this)(n);
        // this.state.model.addNode(n);
        // this.forceUpdate()
      })

  }

  addMenuOpenHandle(){
    this.setState({addMenuOpened:true});
  }
  handleClose = () => this.setState({open: false});
  handleClearEventsLists = () => {
    this.setState({...this.state,eventList:[]});
  }

  renderMenu(){
    return _.map(zoo,(group)=>{
      let items =  _.map(group.items, (node)=>{
        return <MenuItem onTouchTap={this.addClickHandle.bind(this,node)} key={node.config.type}>{node.config.type}</MenuItem>
      });
      return <Card key={group.label}>
        <CardHeader
          title={group.label}
          style={{backgroundColor: 'rgba(0,0,0,0.3)'}}
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardText expandable={true} style={{padding:'0px 0px 10px 0px'}}>
          {items}
        </CardText>
      </Card>
      });
  };

  paramChange(param, key, event, val){
    console.log('paramChange', param ,':', key, '::', val, ';;;', TYPES.params[param.type],TYPES.params[param.type](val));
    let selectedNode = _.cloneDeep(this.state.selectedNode);
    let p = selectedNode.extras[key];
    p.value = TYPES.params[param.type](val);
    this.setState({...this.state,selectedNode});
  }

  paramChangeWithHttpSend(param, key, event, val){
    let selectedNode = _.cloneDeep(this.state.selectedNode);
    let p = selectedNode.extras[key];
    console.log('=-=-=-',p,val);
    p.value = TYPES.params[param.type](val);

    this.setState({...this.state,selectedNode:selectedNode});
    console.log('=-=-=-', selectedNode, this.state.selectedNode,":::", {extras:{[key]:{value:val}}});
    API.httpPut('/nodes/'+this.state.selectedNode.id,{extras:{[key]:{value:val}}})
  }


  keydownPrevent(param, type, event, en) {
    console.log(' keydownPrevent > ', en, ":", param,":",event.keyCode);
    if(event.keyCode==13){
      console.log('ENTER', this.state.selectedNode)
      API.httpPut('/nodes/'+this.state.selectedNode.id,this.state.selectedNode.serialize())
    }
    event.preventDefault(); // Let's stop this event.
    event.stopPropagation(); // Really this time.
    event.nativeEvent.stopImmediatePropagation();

    // let val = '';
    // if(type === 'number'){
    //   val = _.isNaN(+event.key)?'':event.key
    // }else if(type === 'string'){
    //   val = ( event.key.length === 1 && event.key.match(/[a-zA-Z]/i))?'':event.key
    // }else {
    //
    // }
    // param.value = (param.value || '') + val;
    // this.forceUpdate()
  }
  renderParam(param, key){
    switch(param.type){
      case 'int':
      case 'double':
        return <div key={key}>
          <TextField
            fullWidth={true}
            onChange={this.paramChange.bind(this, param, key)}
            onKeyUp={this.keydownPrevent.bind(this, param, param.type)}
            floatingLabelText={param.label}
            type="number"
            value={param.value}
          />
        </div>
        break;
      case 'string':
        if(param.enum){

        }else{
          return <div key={key}>
            <TextField
              fullWidth={true}
              onChange={this.paramChange.bind(this, param, key)}
              onKeyUp={this.keydownPrevent.bind(this, param, param.type)}
              floatingLabelText={param.label}
              type="text"
              value={param.value}
            />
          </div>
        }

        break;
      case 'boolean':
        return <div key={key}>
          <Toggle
            label={param.label}
            onToggle={this.paramChangeWithHttpSend.bind(this, param, key)}
            toggled={param.value}
          />
        </div>
        break;

      default:
        return <div key={key}>{param.label}({param.type}): [{param.default}]</div>
        break;
    }

  }

  renderInspector(){
    let name, id;
    let cont = [];
    if(this.state.selectedNode){
      name = this.state.selectedNode.name;
      id = this.state.selectedNode.id;
      cont = _.map(this.state.selectedNode.extras, (param, key) => {
        return this.renderParam(param, key)
      })
    } else {
      name = 'stage ';
    }

    return (
      <div>
        <div style={{padding:'0px 0px', backgroundColor:'rgba(0,0,0,0.3)'}}>
          <div style={{padding:'20px'}}>
            <h2 style={{marginBottom: '0px'}}>{name}</h2>
            <div style={{fontWeight: '100', fontSize: '11px', opacity: '0.3'}}>{id}</div>
          </div>

      </div>
      <div style={{padding:'20px'}}>
        {cont}
      </div>
    </div>)
  }

  render() {
    const { loading, error, repos } = this.props;
    const reposListProps = {
      loading,
      error,
      repos,
    };

    return (
      <div >
        <Drawer width={250}
                docked={true}
                openSecondary={true}
                onRequestChange={(open) => this.setState({inspectorOpened:open})}
                open={this.state.inspectorOpened} >
          {this.renderInspector()}
        </Drawer>
        <Drawer width={200}
                docked={false}
                openSecondary={false}
                onRequestChange={(open) => this.setState({addMenuOpened:open})}
                open={this.state.addMenuOpened} >
          {this.renderMenu()}
        </Drawer>
        <Header></Header>
        <div>
          <FloatingActionButton style={FABstyle} onClick={this.addMenuOpenHandle.bind(this)}>
            <ContentAdd />
          </FloatingActionButton>
        </div>



        {this.renderD()}
      </div>
    );
  }
}
// // //
// <Console engine={this.state.engine}
//          model={this.state.model}
//          onClearEventsList={this.handleClearEventsLists.bind(this)}
//          eventsList={this.state.eventList}/>


HomePage.propTypes = {
  loading: React.PropTypes.bool,
  error: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  repos: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
  onSubmitForm: React.PropTypes.func,
  username: React.PropTypes.string,
  onChangeUsername: React.PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: (evt) => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
